# -*- python -*-
# ex: set syntax=python:

from buildbot.buildslave import BuildSlave
from buildbot.changes.filter import ChangeFilter
from buildbot.changes.pb import PBChangeSource
from buildbot.config import BuilderConfig
from buildbot.process.factory import BuildFactory
from buildbot.process.properties import WithProperties
from buildbot.status.html import WebStatus
from buildbot.status.web.auth import BasicAuth
from buildbot.status.web.authz import Authz
from buildbot.status.words import IRC, IRCContact
from buildbot.steps.master import MasterShellCommand
from buildbot.steps.source.git import Git
from buildbot.steps.shell import ShellCommand, Compile
from buildbot.steps.transfer import FileUpload
from buildbot.steps.trigger import Trigger
from buildbot.schedulers.basic import AnyBranchScheduler, Dependent
from buildbot.schedulers.triggerable import Triggerable
from buildbot.schedulers.trysched import Try_Userpass

# Monkey patch twisted to allow for bigger netstrings -> bigger try patches
from twisted.protocols.basic import NetstringReceiver
NetstringReceiver.MAX_LENGTH = 10 * 2**20   # 2MB

# Load external configuration files.
import json

BUILDSLAVES_PASSWORDS = json.load(open('buildslaves-passwords.json'))
USERS_PASSWORDS = json.load(open('users-passwords.json'))
CHANGESOURCE_PASSWORD = open('changesource-password.txt').read().strip()
WEBAUTH_PASSWORD = open('webauth-password.txt').read().strip()

class GitNoBranch(Git):
    """Monkey patch for stupid --branch and fetch behavior."""

    def _dovccmd(self, cmd, *a, **kw):
        newcmd = []
        branch = False
        for item in cmd:
            if branch:
                branch = False
            else:
                if item == '--branch':
                    branch = True
                elif item == 'fetch':
                    newcmd.append(item)
                    break
                else:
                    newcmd.append(item)
        return Git._dovccmd(self, newcmd, *a, **kw)

from buildbot.process.buildstep import SUCCESS
class TriggerIfMaster(Trigger):
    """Monkey patch to trigger only if we're on master"""

    def start(self):
        sss = self.build.getAllSourceStamps()
        is_master = False
        for ss in sss:
            if not ss.changes:
                continue
            for ch in ss.changes:
                if ch.properties.getProperty('branchname', None) == 'master':
                    is_master = True

        if is_master:
            return Trigger.start(self)
        else:
            self.running = True
            self.step_status.setText(['(not master)'])
            self.end(SUCCESS)
            return

from buildbot.status.words import maybeColorize
import buildbot.status.words
import requests
def shorten(url):
    try:
        return requests.get('http://ln-s.net/home/api.jsp', params={"url": url}).text.split(' ')[1].split('\n')[0]
    except:
        return "<fail>"

class MyIRCContact(IRCContact):
    def buildFinished(self, builderName, build, results):
        builder = build.getBuilder()

        if (self.bot.categories != None and
            builder.category not in self.bot.categories):
            return

        if not self.notify_for_finished(build):
            return

        builder_name = builder.getName()
        buildnum = build.getNumber()
        buildrevs = build.getRevisions()

        results = self.getResultsDescriptionAndColor(build.getResults())
        if self.reportBuild(builder_name, buildnum):
            buildurl = self.bot.status.getURLForThing(build)
            r = "build %s on %s: [%s] %s" % (
                buildrevs, builder_name, maybeColorize(' '.join(build.getText()), results[1], self.useColors),
                buildurl
            )

            self.send(r)
buildbot.status.words.IrcStatusBot.contactClass = MyIRCContact

def make_dolphin_win_build(build_type, arch, wip=False, debug=False):
    msarch = "Win32" if arch == "x86" else "x64"
    f = BuildFactory()

    f.addStep(GitNoBranch(repourl="https://code.google.com/p/dolphin-emu/",
                          progress=True, mode="incremental"))

    f.addStep(ShellCommand(command=["del", "Binary\\%s\\cg.dll" % msarch,
                                    "Binary\\%s\\cgGL.dll" % msarch],
                           logEnviron=False,
                           description="cleaning cg",
                           descriptionDone="cg clean"))

    branch = WithProperties("%s", "branchname")
    f.addStep(Compile(command=["msbuild.exe", "/p:Platform=%s" % msarch,
                               "/p:Configuration=%s" % build_type],
                      env={ "DOLPHIN_BRANCH": branch },
                      workdir="build/Source",
                      description="building",
                      descriptionDone="build",
                      haltOnFailure=True))

    build_descr = WithProperties("%s-%s", "branchname", "shortrev")
    f.addStep(ShellCommand(command=["C:\\scripts\\storesymbols.bat", "Binary\\%s\\Dolphin.pdb" % msarch,
                                    "Dolphin %s" % arch, build_descr],
                           logEnviron=False,
                           description="extracting symbols",
                           descriptionDone="symbols extraction"))

    f.addStep(ShellCommand(command=["xcopy", "Binary\\%s" % msarch,
                                    "Dolphin-%s" % arch, "/S", "/I"],
                           logEnviron=False,
                           description="copying output",
                           descriptionDone="output copy"))

    f.addStep(ShellCommand(command=["del", "Dolphin-%s\\Dolphin.pdb" % arch,
                                    "Dolphin-%s\\DSPTool.pdb" % arch,
                                    "Dolphin-%s\\Dolphin.lib" % arch,
                                    "Dolphin-%s\\Dolphin.exp" % arch],
                           logEnviron=False,
                           description="purging pdb",
                           descriptionDone="pdb purge"))

    out_filename = WithProperties("Dolphin-%%s-%%s-%s.7z" % arch, "branchname", "shortrev")
    f.addStep(ShellCommand(command=["7z", "a", "-r", out_filename,
                                    "Dolphin-%s" % arch],
                           logEnviron=False,
                           description="compressing",
                           descriptionDone="compression"))

    if debug:
        fn_arch = "dbg-%s" % arch
    else:
        fn_arch = arch

    if not wip:
        master_filename = WithProperties("/srv/http/dl/builds/dolphin-%%s-%%s-%s.7z" % fn_arch, "branchname", "shortrev")
        url = WithProperties("http://dl.dolphin-emu.org/builds/dolphin-%%s-%%s-%s.7z" % fn_arch, "branchname", "shortrev")
    else:
        master_filename = WithProperties("/srv/http/dl/wips/%%s-dolphin-%%s-%%s-%s.7z" % fn_arch, "author", "branchname", "shortrev")
        url = WithProperties("http://dl.dolphin-emu.org/wips/%%s-dolphin-%%s-%%s-%s.7z" % fn_arch, "author", "branchname", "shortrev")
    f.addStep(FileUpload(slavesrc=out_filename, masterdest=master_filename,
                         url=url, keepstamp=True, mode=0644))

    if not wip and not debug:
        f.addStep(MasterShellCommand(command="/home/buildbot/bin/send_build.py",
                                     env={
                                         "BRANCH": WithProperties("%s", "branchname"),
                                         "SHORTREV": WithProperties("%s", "shortrev"),
                                         "HASH": WithProperties("%s", "revision"),
                                         "AUTHOR": WithProperties("%s", "author"),
                                         "DESCRIPTION": WithProperties("%s", "description"),
                                         "BUILD_TYPE": "win32" if arch == "x86" else "win64",
                                         "BUILD_URL": url
                                     },
                                     description="notifying website",
                                     descriptionDone="website notice"))

    f.addStep(ShellCommand(command=["del", "/F", "/S", "/Q", out_filename],
                           logEnviron=False,
                           description="cleaning up files",
                           descriptionDone="cleanup files"))

    f.addStep(ShellCommand(command=["rmdir", "/S", "/Q", "Dolphin-%s" % arch],
                           logEnviron=False,
                           description="cleaning up dirs",
                           descriptionDone="cleanup dirs"))

    return f

def make_dolphin_osx_build(wip=False):
    f = BuildFactory()

    f.addStep(GitNoBranch(repourl="https://code.google.com/p/dolphin-emu/",
                          progress=True, mode="incremental"))

    f.addStep(ShellCommand(command=["mkdir", "-p", "build"],
                           logEnviron=False,
                           description="mkbuilddir",
                           descriptionDone="mkbuilddir"))

    f.addStep(ShellCommand(command=["cmake", ".."],
                           workdir="build/build",
                           description="configuring",
                           descriptionDone="configure",
                           haltOnFailure=True))

    f.addStep(Compile(command=["make", "-j4"],
                      workdir="build/build",
                      description="building",
                      descriptionDone="build",
                      haltOnFailure=True))

    f.addStep(ShellCommand(command=["hdiutil", "create", "dolphin.dmg",
                                    "-srcfolder", "Binaries/", "-ov",
                                    "-volname", WithProperties("Dolphin %s-%s", "branchname", "shortrev")],
                           workdir="build/build",
                           logEnviron=False,
                           description="packaging",
                           descriptionDone="package"))

    if not wip:
        master_filename = WithProperties("/srv/http/dl/builds/dolphin-%s-%s.dmg", "branchname", "shortrev")
        url = WithProperties("http://dl.dolphin-emu.org/builds/dolphin-%s-%s.dmg", "branchname", "shortrev")
    else:
        master_filename = WithProperties("/srv/http/dl/wips/%s-dolphin-%s-%s.dmg", "author", "branchname", "shortrev")
        url = WithProperties("http://dl.dolphin-emu.org/wips/%s-dolphin-%s-%s.dmg", "author", "branchname", "shortrev")
    f.addStep(FileUpload(slavesrc="build/dolphin.dmg", masterdest=master_filename,
                         url=url, keepstamp=True, mode=0644))

    if not wip:
        f.addStep(MasterShellCommand(command="/home/buildbot/bin/send_build.py",
                                     env={
                                         "BRANCH": WithProperties("%s", "branchname"),
                                         "SHORTREV": WithProperties("%s", "shortrev"),
                                         "HASH": WithProperties("%s", "revision"),
                                         "AUTHOR": WithProperties("%s", "author"),
                                         "DESCRIPTION": WithProperties("%s", "description"),
                                         "BUILD_TYPE": "osx",
                                         "BUILD_URL": url
                                     },
                                     description="notifying website",
                                     descriptionDone="website notice"))

    return f

def make_dolphin_debian_build(package=False, wip=False):
    f = BuildFactory()

    f.addStep(GitNoBranch(repourl="https://code.google.com/p/dolphin-emu/",
                          progress=True, mode="incremental"))

    f.addStep(ShellCommand(command=["mkdir", "-p", "build"],
                           logEnviron=False,
                           description="mkbuilddir",
                           descriptionDone="mkbuilddir"))

    f.addStep(ShellCommand(command=["cmake", ".."],
                           workdir="build/build",
                           description="configuring",
                           descriptionDone="configure",
                           haltOnFailure=True))

    f.addStep(Compile(command=["make", "-j2"],
                      workdir="build/build",
                      description="building",
                      descriptionDone="build",
                      haltOnFailure=True))

    if package:
        f.addStep(TriggerIfMaster(schedulerNames=["pack-ubuntu"]))
    return f

def make_ubuntu_package():
    f = BuildFactory()

    f.addStep(GitNoBranch(repourl="https://code.google.com/p/dolphin-emu/",
                          progress=True, mode="incremental"))

    f.addStep(ShellCommand(command=["tar", "xzf", "/home/buildbot/dolphin-debian.tar.gz"],
                           logEnviron=False,
                           description="untaring debian dir",
                           descriptionDone="untar debian dir",
                           haltOnFailure=True))

    reg = WithProperties("s/VER/%s/", "shortrev")
    f.addStep(ShellCommand(command=["sed", "-i", reg, "debian/changelog"],
                           logEnviron=False,
                           description="updating version",
                           descriptionDone="update version",
                           haltOnFailure=True))

    f.addStep(ShellCommand(command=["dpkg-buildpackage", "-uc", "-us", "-nc", "-b", "-j4"],
                           description="building",
                           descriptionDone="build",
                           haltOnFailure=True))

    chg_file = WithProperties("../dolphin-emu-master_%s_amd64.changes", "shortrev")
    deb_file = WithProperties("../dolphin-emu-master_%s_amd64.deb", "shortrev")
    upl_file = WithProperties("/srv/http/dl/builds/dolphin-master-%s_amd64.deb", "shortrev")
    url = WithProperties("http://dl.dolphin-emu.org/builds/dolphin-master-%s_amd64.deb", "shortrev")
    f.addStep(FileUpload(slavesrc=deb_file, masterdest=upl_file,
                         url=url, keepstamp=True, mode=0644))
    
    f.addStep(ShellCommand(command=["rm", "-rf", chg_file, deb_file, "debian"],
                           description="cleaning up",
                           descriptionDone="clean up"))

    f.addStep(MasterShellCommand(command="/home/buildbot/bin/send_build.py",
                                 env={
                                     "BRANCH": WithProperties("%s", "branchname"),
                                     "SHORTREV": WithProperties("%s", "shortrev"),
                                     "HASH": WithProperties("%s", "revision"),
                                     "AUTHOR": WithProperties("%s", "author"),
                                     "DESCRIPTION": WithProperties("%s", "description"),
                                     "BUILD_TYPE": "ubu",
                                     "BUILD_URL": url
                                 },
                                 description="notifying website",
                                 descriptionDone="website notice"))

    return f

def make_android_package():
    f = BuildFactory()

    f.addStep(GitNoBranch(repourl="https://code.google.com/p/dolphin-emu/",
                          progress=True, mode="incremental"))

    f.addStep(ShellCommand(command=["mkdir", "-p", "build"],
                           logEnviron=False,
                           description="mkbuilddir",
                           descriptionDone="mkbuilddir"))

    f.addStep(ShellCommand(command=["cmake", "-DANDROID=True", "-DANDROID_NATIVE_API_LEVEL=android-14",
                                    "-DCMAKE_TOOLCHAIN_FILE=../Source/Android/android.toolchain.cmake", ".."],
                           env={ "ANDROID_NDK": "/home/buildbot/ndk" },
                           workdir="build/build",
                           description="configuring",
                           descriptionDone="configure",
                           haltOnFailure=True))

    f.addStep(Compile(command=["make", "-j2"],
                      workdir="build/build",
                      description="building",
                      descriptionDone="build",
                      haltOnFailure=True))

    f.addStep(ShellCommand(command=["ant", "-Dsdk.dir=/home/buildbot/adt/sdk",
                                    "-Dndk.dir=/home/buildbot/ndk", "-f", "../Source/Android/build.xml",
                                    "release"],
                           workdir="build/build",
                           description="building java",
                           descriptionDone="java build",
                           haltOnFailure=True))

    master_filename = WithProperties("/srv/http/dl/builds/dolphin-%s-%s.apk", "branchname", "shortrev")
    url = WithProperties("http://dl.dolphin-emu.org/builds/dolphin-%s-%s.apk", "branchname", "shortrev")
    f.addStep(FileUpload(slavesrc="Source/Android/bin/Dolphin Emulator-release-unsigned.apk", masterdest=master_filename,
                         url=url, keepstamp=True, mode=0644))

    return f

abs = AnyBranchScheduler(name="all", builderNames=[
                            "release-win-x86",
                            "release-win-x64",
                            "release-osx-x64",
                            "release-deb-x64",
                            "release-ubu-x64",
                            "release-android",
                         ], treeStableTimer=None)

BuildmasterConfig = {
    'title': 'Dolphin Emulator',
    'titleURL': 'http://code.google.com/p/dolphin-emu/',
    'buildbotURL': 'http://buildbot.dolphin-emu.org/',

    'slaves': [
        BuildSlave("windows", BUILDSLAVES_PASSWORDS["windows"]),
        BuildSlave("osx-lion", BUILDSLAVES_PASSWORDS["osx-lion"]),
        BuildSlave("debian", BUILDSLAVES_PASSWORDS["debian"]),
        BuildSlave("ubuntu", BUILDSLAVES_PASSWORDS["ubuntu"]),
    ],
    'slavePortnum': 9989,

    'change_source': [
        PBChangeSource(user="dolphin", passwd=CHANGESOURCE_PASSWORD),
    ],

    'schedulers': [
        abs,

        Try_Userpass(name="wip", builderNames=[
                         "wip-win-x86",
                         "wip-win-x64",
                         "wip-osx-x64",
                         "wip-deb-x64",
                         "wip-ubu-x64",
                     ], port=8031,
                     userpass=USERS_PASSWORDS.items()),

        Triggerable(name="pack-ubuntu", builderNames=["pack-ubu-x64"]),

        Dependent(name="debug", upstream=abs,
                  builderNames=["debug-win-x64"]),
    ],

    'builders': [
        BuilderConfig(name="release-win-x86", slavenames=["windows"],
                      factory=make_dolphin_win_build("Release", "x86")),
        BuilderConfig(name="release-win-x64", slavenames=["windows"],
                      factory=make_dolphin_win_build("Release", "x64")),
        BuilderConfig(name="release-osx-x64", slavenames=["osx-lion"],
                      factory=make_dolphin_osx_build()),
        BuilderConfig(name="release-deb-x64", slavenames=["debian"],
                      factory=make_dolphin_debian_build()),
        BuilderConfig(name="release-ubu-x64", slavenames=["ubuntu"],
                      factory=make_dolphin_debian_build(package=True)),

        BuilderConfig(name="wip-win-x86", slavenames=["windows"],
                      factory=make_dolphin_win_build("Release", "x86", wip=True)),
        BuilderConfig(name="wip-win-x64", slavenames=["windows"],
                      factory=make_dolphin_win_build("Release", "x64", wip=True)),
        BuilderConfig(name="wip-osx-x64", slavenames=["osx-lion"],
                      factory=make_dolphin_osx_build(wip=True)),
        BuilderConfig(name="wip-deb-x64", slavenames=["debian"],
                      factory=make_dolphin_debian_build(wip=True)),
        BuilderConfig(name="wip-ubu-x64", slavenames=["ubuntu"],
                      factory=make_dolphin_debian_build(wip=True)),

        BuilderConfig(name="pack-ubu-x64", slavenames=["ubuntu"],
                      factory=make_ubuntu_package()),

        BuilderConfig(name="release-android", slavenames=["ubuntu"],
                      factory=make_android_package()),

        BuilderConfig(name="debug-win-x64", slavenames=["windows"],
                      factory=make_dolphin_win_build("Debug", "x64", debug=True)),
    ],

    'status': [
        WebStatus(http_port=8010, authz=Authz(
            auth=BasicAuth([("dolphin", WEBAUTH_PASSWORD)]),
            forceBuild="auth",
            forceAllBuilds="auth",
            pingBuilder="auth",
            gracefulShutdown="auth",
            stopBuild="auth",
            stopAllBuilds="auth",
            cancelPendingBuild="auth",
            stopChange="auth",
            cleanShutdown="auth"
        ), order_console_by_time=True),

        IRC("irc.freenode.net", "DolBuildBot", channels=[
                { "channel": "#dolphin-dev" },
            ], notify_events={
                "failure": True,
            },
            useRevisions=True,
        )
    ],

    'db': {
        'db_url': 'sqlite:///state.sqlite',
    },

    'mergeRequests': False,
}
